#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
#  Create txt file with descriptions fir all ZWI files in a directory.
#  To reduce output, one can only look at a fraction of files.  
#  S.V.Chekanov (KSF)

import gzip,re,sys,os
from time import time
from zipfile import ZipFile
from short_description import *

############## USER SETTINGS ##################################
# giving directory name with ZWI files. 
# You should comment this import if you use your local directory
from common_zwi import *
folderdir = basedirZWI

# max number of files or -1 (all files)
Nmax=-1

# What fraction of files to add to this file?
# assume 10% of random files
onlyFraction=0.1

########################### do not change below ##################
# giving file extensions
ext = ('.zwi')

ns=0
nn=0
files=[]
xlist=[]
# iterating over directory and subdirectory to get desired result
for path, dirc, files in os.walk(folderdir):
    for name in files:
        if name.endswith(ext):
           xfull=path+"/"+name
           #print(xfull)
           xtime=int(os.path.getmtime(xfull))
           xsize=int(os.path.getsize(xfull))
           #print(int(os.path.getmtime(xfull)))
           zwifile=xfull.replace(path+"/","")
           if (xsize<500): 
                 print("Not a ZWI file!",zwifile)
                 continue 
           #print(xtime,xsize,xdir)
           nn=nn+1
           if (onlyFraction<1.0):
             takeevent=False;
             if (nn%int(1/onlyFraction) == 0): takeevent=True
             if (takeevent == False): continue
             ns=ns+1
             unzipped_file = ZipFile(xfull, "r")
             metadata = unzipped_file.read("metadata.json").decode('utf-8')
             js_metadata=json.loads(metadata)
             title=js_metadata["Title"]
             publisher=(js_metadata["Publisher"]).lower()
             # print(xfull)
             description=""
             texthtml="";
             wikitext="";
             articletxt="";
             try:
                texthtml = unzipped_file.read("article.html").decode(encoding="utf-8") 
             except:
                 pass

             try:
                wikitext=unzipped_file.read("article.wikitext").decode(encoding="utf-8") 
             except:
                 pass

             try:
                articletxt=unzipped_file.read("article.txt").decode(encoding="utf-8") 
             except:
                 pass

             unzipped_file.close()
             if (nn %1000 == 0): print("Processed=",nn," selected=",ns)

             #if "Description" in js_metadata:
             #     description=js_metadata["Description"]
             #else:
             #     print("No description=",xfull)
             #     pass; #  os.remove(xfull)
             description= getShortDescription(publisher,wikitext,texthtml,articletxt) ;
             #print(description)
             xlist.append([xtime,zwifile,xfull,title,description,publisher])
             if (Nmax>0):
                   if (nn>Nmax): break

# sort using time
xlist=sorted(xlist,key=lambda x: x[0],reverse=True)

outfile="zwi_description.txt"
f1=open(outfile,"w")

xlong=0
xshort=0
n=0
for j in xlist:
    zwifile=j[1]
    description=j[4]
    publisher=j[5]
    if (len(description)<10): xshort=xshort+1
    if (len(description)>500): xlong=xlong+1
    n=n+1
    if (len(description)>500):
                     description=description[0:500]+" ..long to show .." 
    f1.write( str(n).ljust(8)+" | "+publisher.ljust(18)+" | "+str(zwifile)+" | "+str(description)+"\n")  # printing file name
f1.write('\n')
f1.write("Nr with short description (<10)"+str(xshort)+" Fraction="+str((xshort/float(n))*100)+"%\n")
f1.write("Nr with long  description (>500)="+str(xlong)+" Fraction="+str((xlong/float(n))*100)+"%\n")
f1.write('\n')
f1.close()
print("Create=",outfile)
