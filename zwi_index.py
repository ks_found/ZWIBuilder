#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Create 2-level index files for the ZWI database. All times are in the UTC.  
# To make all index files for all publishers by rescanning ZWI file use: 
#        python3 zwi_index.py -i <path> where <path> points to the directory with ZWI files.
# To update the index of specific publishers and the global index file use:
#        python3 zwi_index.py -i <path> -p "publisher1,publisher2" 
# Use the comma-seprated list of publishers. Note scanning of directories for other publishers will not be performed
#
# This directory should ends as ZWI/en (without / at the end).
# This module can also update the index of a specific publisher  
# 
# S.V.Chekanov, H.Sanger (KSF)
# Version 1.0, Nov 10, 2022
# Version 1.1, Nov 13, 2022
#
import re,sys,os
import glob,gzip
from time import time
import time
from operator import itemgetter
from pathlib import Path
import argparse
from os.path import exists
from datetime import datetime
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# create index file for the ZWI directory
# Optionally, specify a slit with excluded encyclopedias 
def createIndexFiles(basedirZWI, excluded=[]):
  """ Create new index files from the directory basedirZWI, but excluding excluded[] """
  start = time.time()
  outfile=basedirZWI+"/index.csv.gz"
  data=[]
  toremove=basedirZWI+"/";

  nn=0
  print("Initial scan of directories")
  for f in glob.glob(basedirZWI+"/*/*", recursive=False):
    remove=False
    for ex in excluded:
         if (f.find(ex)>-1): remove=True
    if (remove): continue
    if os.path.isdir(f):
          name=f.replace(toremove,"") 
          name=name.strip()
          if (len(name)<2): continue 
          xtime=os.path.getmtime(f) # modification time 
          data.append( [int(xtime),name,f] )
          nn=nn+1

  # sort according to time
  data.sort(key=itemgetter(0),reverse=True)

  dirnumber={}
  dirsizes={}
  print("Making local index files for",nn," encyclopedias")
  nn=0
  xsum=0
  for d in data:
    m_name=d[1]  # short name of the directory 
    xdir=d[2]    # full path 
    d2list=m_name.split("/")
    if len(d2list)>1:
        fname=d2list[1]
    localindex= xdir+".csv.gz"
    print("Creating:"+localindex)
    output = gzip.open(localindex, 'w')
    sdata="";
    fulllist=[]
    # slow
    #flist=glob.glob(xdir+"/*.zwi", recursive=False)
    #sorted_by_mtime_descending = sorted(flist, key=lambda t: -os.stat(t).st_mtime)

    flist=os.scandir(xdir+"/")

    dir_bytes=0
    dir_number=0
    for item in  flist: 
       shortname=item.name
       if (shortname.endswith(".zwi")):
         shortname=shortname[0:len(shortname)-4]
         #path=item.path
         xsize=int(item.stat().st_size) 
         xtime=int(item.stat().st_mtime) # time of last modification. 

         #xtime=int(os.path.getmtime(f))
         #xsize=int(os.path.getsize(f))
         #shortname=Path(f).stem
         fulllist.append(str(xtime)+"|"+str(xsize)+"|"+shortname+"\n")
         nn=nn+1
         xsum=xsum+xsize
         dir_bytes=dir_bytes+xsize
         dir_number=dir_number+1

    dirnumber[m_name]=dir_number
    dirsizes[m_name]=int(dir_bytes) # evaluate correct size of the directory
    fulllist.sort(reverse=True)
    print(" -- ",len(fulllist)," entries")
    for line in fulllist:
         output.write(line.encode('utf-8')) 
    output.close()
    os.system("chmod 777 "+localindex)

  print("")
  print("Total files=",nn," Total size=",xsum/1073741824.0," GB")


  nn=0
  totsize=0
  print("Write new global index:",outfile)
  output = gzip.open(outfile, 'wb')
  xlines=""
  for d in data:
    xtime=str(d[0])
    xname=str(d[1])
    xnumber=dirnumber[xname]  # number of files 
    xsize=dirsizes[xname] # size of this directory
    # clean-up
    xname=xname.replace(basedirZWI,"")
    if (xname.startswith("/")): xname=xname[1:len(xname)]
    if (xname.endswith("/")): xname=xname[0:len(xname)-1]
    totsize=totsize+xsize 
    line=str(xtime)+"|"+str(xsize)+"|"+str(xnumber)+"|"+xname+"\n"
    output.write(line.encode('utf-8'))
    nn=nn+1
  output.close()
  os.system("chmod 777 "+outfile)
  print("Total encyclopedias=",nn," Total size=",totsize/1073741824.0," GB")
  end = time.time()
  print("Used time=",end - start)


###################################################
##################  writeIndexFiles ###############
def writeIndexFiles(basedirZWI,globalIndex,publist=[]):
  """ Write new index files to the disk for the list of  publishers (or all by default)"""
  outfile=basedirZWI+"/index.csv.gz"

  nn=0
  totsize=0
  print("Update global index:",outfile)
  output = gzip.open(outfile, 'wb')
  xlines=""
  for key in globalIndex:
    d=globalIndex[key]
    xtime=str(d[0])
    xsize=str(d[1])
    xnumber=str(d[2])
    totsize=totsize+int(xsize) 
    line=str(xtime)+"|"+str(xsize)+"|"+str(xnumber)+"|"+key+"\n"
    output.write(line.encode('utf-8'))
    nn=nn+1
  output.close()
  os.system("chmod 777 "+outfile)
  print("Total encyclopedias=",nn," Total size=",totsize/1073741824.0," GB")

  print("Update publisher's index files")
  for key in globalIndex:
    keys=key.split("/")
    consider=False
    if (keys[0] in publist):
                    consider=True;
    if (consider==False): continue 
      
    localindex=basedirZWI+"/"+key+".csv.gz"
    print("  Creating:"+localindex)
    output = gzip.open(localindex, 'w')
    entries=globalIndex[key]
    filllist=entries[3]
    print("  Write=",len(filllist),"entries from publisher",key)
    for en in filllist:
        txt=str(en[0])+"|"+str(en[1])+"|"+en[2]+"\n"
        output.write(txt.encode('utf-8'))
    output.close()
    os.system("chmod 777 "+localindex)

######################################################
# Update index files for the publisher
#####################################################
def updateIndexFiles(basedirZWI, publist=[]):
   """ Update index files for the list of specific publisher. No scanning of other publishers """
   globalIndex=getIndex(basedirZWI) # get global index first
   #print("Update index:",basedirZWI,"for publishers:",publist)

   pubkeys=[]
   pubdata=[] 
   # fetch the publisher
   for key in globalIndex: 
        keys=key.split("/")
        if (keys[0] in publist):
              pubdata.append(globalIndex[key]) 
              pubkeys.append(key) # name of the publisher/directory 

   if (len(pubdata)==0): 
       print("The publishers:",publist,"are not found")
       sys.exit()

   print("Following publishers to be updated", pubkeys)
   dirnumber={}
   dirsizes={}
   xsum=0
   # now scan all the  files for this list of publishers
   for xdir in pubkeys:
      flist=os.scandir(basedirZWI+"/"+xdir+"/")
      dir_bytes=0
      dir_number=0
      fulllist=[]
      for item in  flist:
        shortname=item.name
        if (shortname.endswith(".zwi")):
          shortname=shortname[0:len(shortname)-4]
          xsize=int(item.stat().st_size)
          xtime=int(item.stat().st_mtime) # time of last modification.
          fulllist.append([xtime,xsize,shortname])
          xsum=xsum+xsize
          dir_bytes=dir_bytes+xsize
          dir_number=dir_number+1

      # sort according to date
      fulllist.sort(key=itemgetter(0),reverse=True)

      # most recent time:
      recent=fulllist[0][0]
      globalIndex[xdir]=[recent,dir_bytes,dir_number, fulllist]
      print("  - Files updated:",len(fulllist),"Publisher:",xdir,"Updated on:",datetime.fromtimestamp(recent))

   # write new index files
   writeIndexFiles(basedirZWI,globalIndex,publist)
   return


# Update index files for the publisher 
# Read global index files for all publishers 
# The structure is: dic{key}=list
def getIndex(basedirZWI):
      """ Return global index of all publishers """

      outfile=basedirZWI+"/index.csv.gz"
      if (exists(outfile) == False): 
                             print("File not found",outfile)
                             sys.exit()
      print("Read:",outfile)

      # read main index file
      mainIndex={}
      with gzip.open(outfile,'rt') as f:
        for line in f:
           line=line.replace("\n","")
           lines=line.split("|")
           if (len(lines) !=4):  
               print("Problems in reading index file:",outfile)
               sys.exit()
           mainIndex[lines[3]]=[ lines[0],lines[1], lines[2] ] 

      ntot=0
      globalIndex={}
      # read data for specific publishers
      for key in mainIndex:
          pub_index=basedirZWI+key+".csv.gz" 
          if (exists(pub_index) == False):
            print("File not found",pub_index," index is not valid!")
            return 
          articles=[]
          with gzip.open(pub_index,'rt') as f:
            for line in f:
              line=line.replace("\n","")
              lines=line.split("|")
              if (len(lines) !=3):
                  print("Problems reading index file:",pub_index)
                  sys.exit()
              articles.append(lines)
              # print(lines)
              ntot=ntot+1
          #print(key,"gets",len(articles)," articles")
          xmap=mainIndex[key]
          globalIndex[key] = [xmap[0],xmap[1],xmap[2],articles]

      print("The size of main index is {} articles".format(ntot),"in",len(globalIndex),"publishers")
      return globalIndex

###############################################################
################## remove ZWI files ###########################
###############################################################
def deleteZwiFiles(basedirZWI, zwiFiles=[],  publist=[]):
   """ Remove ZWI files from the disk and the index. Specify the list of ZWI files (without extension) and the publisher from which the files should be removed.  """
   globalIndex=getIndex(basedirZWI)
   pubkeys=[]
   pubdata=[]
   # fetch the publisher
   for key in globalIndex:
        keys=key.split("/")
        if (keys[0] in publist):
              pubdata.append(globalIndex[key])
              pubkeys.append(key) # name of the publisher/directory 

   if (len(pubdata)==0):
       print("i\nError: The publishers:",publist,"are not found")
       return 

   err=""
   ntot=0
   nremove=0
   # now scan all entries in the local index and remove the ZWI file
   # we also remove file if there is anything to remove
   for xdir in pubkeys:
          pub_index=basedirZWI+"/"+xdir+".csv.gz"
          # print("Read:",pub_index) 
          if (exists(pub_index) == False):
            err="\nError: File not found"+pub_index+" Abort removal for ZWI files"; print( bcolors.WARNING + err + bcolors.ENDC ) 
            return
          articles=[]
          with gzip.open(pub_index,'rt') as f:
            for line in f:
              line=line.replace("\n","")
              lines=line.split("|")
              if (len(lines) !=3):
                  err="\nError:Problem for reading the index file:"+pub_index; print( bcolors.WARNING + err + bcolors.ENDC ) 
                  return 
              zwiname=lines[2].strip()
              if (zwiname in zwiFiles):
                  zwipath=basedirZWI+"/"+xdir+"/"+zwiname+".zwi"
                  if (exists(zwipath)):
                       os.rename(zwipath,basedirZWI+"/trash/"+zwiname+".zwi") 
                       print ("  -> ZWI file:",xdir+"/"+zwiname+".zwi removed to /trash/")
                       nremove=nremove+1 
                       continue 
                  else:
                       err="\nWarning: File:"+xdir+"/"+zwiname+".zwi was found in the index but not on the disk."
                       nremove=nremove+1 
                       continue 

              articles.append(lines)
              ntot=ntot+1

          if (len(err)>1):
                  print( bcolors.WARNING + err + bcolors.ENDC ) 
          if (nremove==0):
                  err="\nWarning: Nothing to remove. Index file is unchanged"
                  print( bcolors.WARNING + err + bcolors.ENDC ) 
                  return

          #print(key,"gets",len(articles)," articles")
          xmap=globalIndex[xdir]
          globalIndex[xdir] = [xmap[0],xmap[1],xmap[2],articles]

   print("Nr of ZWI files removed:",nremove)
   print("The size of main index is {} articles".format(ntot),"in",len(globalIndex),"publishers")

   # now write the global index file
   writeIndexFiles(basedirZWI,globalIndex,publist)
   return


if __name__ == "__main__":

  #### input parameters ####
  kwargs = {}
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", '--input', help="Input ZWI directory that ends as ZWI/en to create index files by rescanning directories")
  parser.add_argument("-p", '--publishers', help="Update the index for the list of publishers by rescanning their directories. Global index will be updated too. The names of publishers should be separated  by the commas")
  parser.add_argument("-d", '--delete', help="Delete ZWI files and update the index. Global index will be updated too. The names of zwi file  should be separated by the commas. Do not add extensions. You must to add one publisher from where the ZWI files should be remove. This operation  does not involve re-scanning directories")

  args = parser.parse_args()
  basedirZWI=""
  if(args.input != None):
         basedirZWI=args.input
  if(args.publishers != None and args.delete == None):
      if (len(args.publishers)>0):
                print("Update the existing index files for the publishers:",args.publishers.split(",")) 
  if(args.delete != None):
     if (len(args.delete)>0):
        if(args.publishers != None): 
            # if you made mistake and added .zwi extension, we correct for this
            filesRemove=[]
            splitted=args.delete.split(",")
            for f in splitted:
                if f.endswith(".zwi"):
                    filesRemove.append( f.replace(".zwi","") )
                else: filesRemove.append( f.strip())
            print("Remove the list of ZWI files:",filesRemove,"from publishers:",args.publishers.split(",")) 
            deleteZwiFiles(basedirZWI, filesRemove, args.publishers.split(",")) 
            sys.exit() 
        else:       
            err="\nError: These ZWI will be removed:"+args.delete.split(",")+" but the publisher is unknown. Specify the publisher!" 
            print( bcolors.WARNING + err + bcolors.ENDC )
            sys.exit()

  if (len(basedirZWI)<2):
    print("Did not specify the input ZWI directory? This should be the full path that ends with ZWI/en. Use --help")
    sys.exit()

  # excluded (optional) 
  excluded=["en/sep/","en/greatplains/","en/jewishenc/","en/whe/"]

  # now make new index files for all publishers 
  if (args.publishers == None): createIndexFiles(basedirZWI, excluded) 

  #update a specific publisher
  if (args.publishers != None):
       publist = args.publishers.split(",")
       updateIndexFiles(basedirZWI, publist)
