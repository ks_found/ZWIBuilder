#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is used to update meta description of ZWI files.
#
# @version 1.0, June 8, 2022 
# S.V.Chekanov, L.Sanger, C.Gribneau  
# Organzation: KSF
# 
from zipfile import ZipFile
import re,os,sys,json
from os.path import exists
import hashlib
from datetime import datetime
import requests
import argparse
from  short_description import *
import subprocess

def zwiUpdate(zfile):
       """ Main function to update ZWI file"""

       unzipped_file = ZipFile(zfile, "r")
       metadata = unzipped_file.read("metadata.json")
       media = unzipped_file.read("media.json")
       articlehtml = unzipped_file.read("article.html")
       wikitext = unzipped_file.read("article.wikitext")
       articletxt = unzipped_file.read("article.txt")

       articlehtml = articlehtml.decode('utf-8')
       wikitext  =   wikitext.decode('utf-8')
       articletxt  =  articletxt.decode('utf-8')


       #print(metadata)
       js_metadata=json.loads(metadata)
       js_media=json.loads(media)
       #print(js_media)
       js_content=js_metadata["Content"]


       atitle=js_metadata["Title"]
       asource=js_metadata["Publisher"]

       ## find correct topics
       topics=[]
       # topics are set for HandWiki only
       if (asource=="handwiki"):
            tt=atitle.split(":",1);
            if (len(tt)>1):
                    topics.append(tt[0])
                    atitle=tt[1]
       shortdesc=getWikiShortDescription(asource,wikitext)
       if (len(shortdesc)>3):
                    topics.append( shortdesc )

       # update topics
       js_metadata["Topics"]=topics


       ############### title #################################
       ftitle=atitle.replace("_"," ")
       js_metadata["Title"]=ftitle 

       ##############################  Description ####################
       # short description as implemented by Larry
       xff=open("/tmp/tmp.html","w")
       xff.write(articlehtml)
       xff.close()

       cmd="ruby "+script_dir+"/tools/desc_getter/desc_getter.rb /tmp/tmp.html "+asource
       # print(cmd)
       p = subprocess.Popen(cmd,  stdout=subprocess.PIPE, shell=True)
       (output, err) = p.communicate()
       p_status = p.wait()
       description=output.decode()

       # Get 2 sentances
       if (description != None):
         if (len(description)>10):
           description=' '.join(re.split(r'(?<=[.;?!])\s', description)[:2])
           description = description.replace("\n","")
           description=description.strip();
           if (description.endswith(".") == True): description=description[0:len(description)-1] # no full stop
           if (description.endswith(":") == True): description=description[0:len(description)-1] # no full stop

       if (description  != None):
          if len(description)>9:
              print (" :: Ruby short description :: ",description);
       # Fall back
       if (description == None):
         description= getShortDescription(asource,wikitext,articlehtml,articletxt) ;
       if (len(description)<10):
         description= getShortDescription(asource,wikitext,articlehtml,articletxt) ;
         print (" :: Python short description :: ",description);

       unzipped_file.close()

       # Directly from dictionary
       with open('metadata.json', 'w') as outfile:
              json.dump(js_metadata, ensure_ascii=False, outfile,indent=4)


       print("Update metadata.json in  ZWI file")
       os.system("zip -u \""+zfile+"\" metadata.json")
       os.remove("metadata.json")




#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quiet', action='store_true', help="don't show verbose")
parser.add_argument("-z", '--zwi', help="Input ZWI file. Output will be the same as input")

args = parser.parse_args()
args.verbose = not args.quiet

script_dir=os.path.dirname(os.path.realpath(__file__))
print("From = ",script_dir)
print(" : Input ZWIfile =",args.zwi)
print(" : Is verbose   =",args.verbose)
   
zwiUpdate(args.zwi)



